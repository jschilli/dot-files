#!/bin/sh

# Install rustup
 curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
# Template installs
cargo install cargo-generate
cargo install flamegraph

# Embedded rust stuff
cargo install cargo-binutils

rustup component add llvm-tools-preview
cargo install probe-run
cargo install flip-link
# OpenOCD
brew install openocd
rustup target add thumbv7m-none-eabi
# Cortex M4
rustup target add thumbv7em-none-eabi
rustup target add thumbv7em-none-eabihf
