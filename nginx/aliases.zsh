alias nginx-up="sudo nginx -c /usr/local/etc/nginx/nginx.conf"
alias nginx-up-smart="sudo nginx -c /usr/local/etc/nginx/nginx.conf.smart"
alias nginx-up-q2config="sudo nginx -c /usr/local/etc/nginx/nginx.conf.q2config"
alias nginx-down="sudo nginx -s stop"
