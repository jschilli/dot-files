#!/bin/sh

# Load npm_globals, add the default node into the path, initialize rbenv.
npm install -g jshint
npm install -g jsctags
npm install -g karma
npm install -g node-inspector
npm install -g plato
npm install -g cordova


