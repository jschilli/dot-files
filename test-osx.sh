#!/usr/bin/env bash

# Install command line tools
xcode-select -p
if [[ $? -ne 0 ]]; then
    xcode-select --install
fi

# A full installation of Xcode.app is required to compile macvim.
# Installing just the Command Line Tools is not sufficient.
xcodebuild -version
if [[ $? -ne 0 ]]; then
    # TODO: find a way to install Xcode.app automatticaly
    # See: http://stackoverflow.com/a/18244349

    # Accept Xcode license
    sudo xcodebuild -license
fi

# Update all OSX packages
sudo softwareupdate -i -a

# Install Homebrew if not found
brew --version
if [[ $? -ne 0 ]]; then
    # Clean-up failed Homebrew install
    rm -rf "/usr/local/Cellar" "/usr/local/.git"
    # Install Homebrew
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi
brew update
brew upgrade brew-cask
brew upgrade

# Include duplicates packages
brew tap homebrew/dupes

# Install Cask
brew tap caskroom/cask
brew install brew-cask

# Install OSX system requirements
# brew cask install --force xquartz

# Install common packages
brew install apple-gcc42

brew install ack
brew install dockutil
brew install exiftool
brew install git
brew install git-flow
brew install graphicsmagick
brew install imagemagick
brew install jpeg
brew install libpng
brew install libtiff
brew install libyaml
brew install markdown
brew install multimarkdown
brew install nginx
brew install nvm
brew install node
brew install npm
brew install pkg-config
brew install readline
brew install ruby
brew install todo-txt
brew install uncrustify
brew install hyperfine

brew install s3cmd
brew install wget
brew install zsh
brew install highlight
brew install ffmpeg
brew install gifsicle
brew install ripgrep

brew install hub
brew install ssh-copy-id
brew install watch
brew install webkit2png
brew tap railwaycat/emacsmacport && brew install emacs-mac --with-imagemagick --with-modern-icon --with-modules
osascript -e 'tell application "Finder" to make alias file to POSIX file "/opt/homebrew/opt/emacs-mac/Emacs.app" at POSIX file "/Applications"'
# htop-osx requires root privileges to correctly display all running processes.
sudo chown root:wheel "$(brew --prefix)/bin/htop"
sudo chmod u+s "$(brew --prefix)/bin/htop"



# Install vim
brew install lua --completion
brew install cscope
VIM_FLAGS="--with-python --with-lua --with-cscope --override-system-vim"
brew install macvim "$VIM_FLAGS"
# Always reinstall vim to fix Python links.
# See: https://github.com/yyuu/pyenv/issues/234
brew reinstall vim "$VIM_FLAGS"

# Clean things up
brew linkapps
brew cleanup
brew prune
brew cask cleanup

# Install nvm
curl https://raw.githubusercontent.com/creationix/nvm/v0.39.1/install.sh | bash

