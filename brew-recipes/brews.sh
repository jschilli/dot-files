#!/bin/sh
brew install ack
brew install autoconf
brew install automake
brew install bash-completion
brew install freetype
brew install gdbm
brew install git
brew install git-flow
brew install graphicsmagick
brew install grc
brew install htmldoc
brew install imagemagick
brew install jpeg
brew install libpng
brew install libtiff
brew install libyaml
brew install markdown
brew install multimarkdown
brew install nginx
brew install node
brew install npm
brew install pcre
brew install phantomjs
brew install pkg-config
brew install readline
brew install ruby
brew install starship
brew install todo-txt
brew install uncrustify

brew install s3cmd
brew install wget
brew install zsh

brew install vim
brew install ctags
brew install ios-webkit-debug-proxy
brew install highlight
brew install ffmpeg
brew tap phinze/homebrew-cask
brew install brew-cask
brew cask install iterm2
brew cask install google-chrome

#brew cask install x-quartz
#open /usr/local/Cellar/x-quartz/2.7.4/XQuartz.pkg
# click through the stuff
brew install gifsicle
brew install ios-sim
brew install the_silver_searcher
brew install git-extras

brew install starship
brew tap homebrew/cask-fonts && brew install --cask font-sauce-code-pro-nerd-font