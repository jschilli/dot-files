#!/usr/bin/env bash

# Install command line tools
xcode-select -p
if [[ $? -ne 0 ]]; then
    xcode-select --install
fi

# A full installation of Xcode.app is required to compile macvim.
# Installing just the Command Line Tools is not sufficient.
xcodebuild -version
if [[ $? -ne 0 ]]; then
    # TODO: find a way to install Xcode.app automatticaly
    # See: http://stackoverflow.com/a/18244349

    # Accept Xcode license
    sudo xcodebuild -license
fi

# Update all OSX packages
sudo softwareupdate -i -a

# Install Homebrew if not found
brew --version
if [[ $? -ne 0 ]]; then
    # Clean-up failed Homebrew install
    rm -rf "/usr/local/Cellar" "/usr/local/.git"
    # Install Homebrew
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi
brew update
brew upgrade brew-cask
brew upgrade

# Include duplicates packages
brew tap homebrew/dupes

# Install Cask
brew tap caskroom/cask
brew install brew-cask

# Install OSX system requirements
# brew cask install --force xquartz

# Install common packages
brew install apple-gcc42

brew install ack
brew install dockutil
brew install exiftool
brew install git
brew install git-flow
brew install graphicsmagick
brew install imagemagick
brew install jpeg
brew install libpng
brew install libtiff
brew install libyaml
brew install markdown
brew install multimarkdown
brew install nginx
brew install node
brew install npm
brew install tag
brew install pkg-config
brew install readline
brew install ruby
brew install todo-txt
brew install uncrustify

brew install s3cmd
brew install wget
brew install zsh
brew install ios-webkit-debug-proxy
brew install highlight
brew install ffmpeg
brew install gifsicle
brew install ios-sim
brew install the_silver_searcher

brew install hub
brew install ssh-copy-id
brew install watch
brew install webkit2png

# htop-osx requires root privileges to correctly display all running processes.
sudo chown root:wheel "$(brew --prefix)/bin/htop"
sudo chmod u+s "$(brew --prefix)/bin/htop"

brew cask install --force chromium
brew cask install --force dropbox
brew cask install --force flux
brew cask install iterm2
brew cask install google-chrome

# click through the stuff
# Install QuickLooks plugins
# Source: https://github.com/sindresorhus/quick-look-plugins
brew cask install --force betterzipql
brew cask install --force cert-quicklook
brew cask install --force epubquicklook
brew cask install --force qlcolorcode
brew cask install --force qlmarkdown
brew cask install --force qlprettypatch
brew cask install --force qlstephen
brew cask install --force quicklook-csv
brew cask install --force quicklook-json
brew cask install --force suspicious-package
brew cask install --force webp-quicklook
qlmanage -r

# Install vim
brew install lua --completion
brew install cscope
VIM_FLAGS="--with-python --with-lua --with-cscope --override-system-vim"
brew install macvim "$VIM_FLAGS"
# Always reinstall vim to fix Python links.
# See: https://github.com/yyuu/pyenv/issues/234
brew reinstall vim "$VIM_FLAGS"

# Clean things up
brew linkapps
brew cleanup
brew prune
brew cask cleanup

# Install nvm
curl https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash

npm install -g ember-cli
npm install -g bower

