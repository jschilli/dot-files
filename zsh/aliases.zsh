alias reload!='. ~/.zshrc'
alias k='karma start tools/karma.conf.js --browsers PhantomJS --reporters progress,growl'
alias kwc='karma start tools/karma.conf.js --browsers PhantomJS --reporters spec,growl,coverage'
alias kc='karma start tools/karma.conf.js --browsers Chrome --reporters progress,growl --single-run'
alias kcc='karma start tools/karma.conf.js --browsers ChromeCanary --reporters progress,growl --single-run'
alias kf='karma start tools/karma.conf.js --browsers Firefox --reporters progress,growl --single-run'
alias ks='karma start tools/karma.conf.js --browsers Safari --reporters progress,growl --single-run'
alias ko='karma start tools/karma.conf.js --browsers Opera --reporters progress,growl --single-run'
alias ka='karma start tools/karma.conf.js --reporters progress,growl --single-run'
alias kas='karma start tools/karma.conf.js --reporters progress,growl '
alias ck='karma start tools/coverage.conf.js --browsers PhantomJS --single-run '
alias kk='./node_modules/karma/bin/karma start tools/karma.conf.js --no-auto-watch --browsers PhantomJS -reporters progress,coverage,growl --single-run'
alias kspec='karma start tools/karma.conf.js --browsers PhantomJS --reporters spec,growl '



alias q2='cd ~/dev/q2'
alias w='cd ~/dev/personal/Sites/wordpress'

alias bet='grunt test'
alias beb='grunt build'

alias krup='killall -9 rackup'
alias cup='krup || bup'
alias bab='grunt watch'
alias sm='sprint --me -e'
alias se='sprint -e'

#alias vim="/Applications/MacVim.app/Contents/MacOS/Vim"
alias get_idf='. $HOME/esp/esp-idf/export.sh'