eval "$(npm completion 2>/dev/null)"

# Install and save to dependencies
alias npms="npm i -S "
#
# # Install and save to dev-dependencies
alias npmd="npm i -D "

alias npmt='echo now \`nt\` thx for playing'
alias nt='npm test'
alias nta='npm run test:all'
alias ntc='npm run test:ci'
alias nrc='npm run compile'
alias npmb='npm build'
alias npmo='npm run-script doc'
