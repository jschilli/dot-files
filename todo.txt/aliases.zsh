# todo.sh: https://github.com/ginatrapani/todo.txt-cli
function t() { 
  if [ $# -eq 0 ]; then
    todo.sh ls
  else
    todo.sh $*
  fi
}

alias tl="todo.sh list"
alias ta="todo.sh add" 
alias td="todo.sh do"
alias tls="todo.sh list | sort"
alias tnav="todo.sh nav"

alias n="t ls +next"
