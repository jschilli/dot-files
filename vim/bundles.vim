" Setting up Vundle - the vim plugin bundler
let iCanHazVundle=1
let vundle_readme=expand('~/.vim/bundle/vundle/README.md')
if !filereadable(vundle_readme)
	echo "Installing Vundle.."
	echo ""
	silent !mkdir -p ~/.vim/bundle
	silent !git clone https://github.com/gmarik/vundle ~/.vim/bundle/vundle
	let iCanHazVundle=0
endif
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()
Bundle 'gmarik/vundle'
"Add your bundles here
Bundle 'altercation/vim-colors-solarized' 
Bundle 'Lokaltog/vim-distinguished'
" Global Plugins
Bundle 'scrooloose/nerdtree'
Bundle 'flazz/vim-colorschemes'
Bundle 'rgarver/Kwbd.vim'
Bundle 'rizzatti/funcoo.vim'
Bundle 'rizzatti/dash.vim'
"
" General Programming stuff
Bundle 'Syntastic' 
Bundle 'tpope/vim-fugitive' 
Bundle 'nathanaelkane/vim-indent-guides'
Bundle 'git://git.wincent.com/command-t.git'
Bundle 'airblade/vim-gitgutter'
Bundle 'ack.vim'
Bundle 'Raimondi/delimitMate'
Bundle 'scrooloose/nerdcommenter'
Bundle 'majutsushi/tagbar'
Bundle 'MarcWeber/vim-addon-mw-utils'
Bundle 'tomtom/tlib_vim'
Bundle 'SirVer/ultisnips'
"Bundle 'garbas/vim-snipmate'
Bundle 'zerowidth/vim-copy-as-rtf'

Bundle 'editorconfig/editorconfig-vim'
"

" Javascript enhancements
"
Bundle 'pangloss/vim-javascript'
"Bundle 'jelera/vim-javascript-syntax'
"Bundle 'marijnh/tern_for_vim'
Bundle 'maksimr/vim-jsbeautify' 
Bundle 'einars/js-beautify' 
Bundle 'millermedeiros/vim-esformatter'
Bundle 'bling/vim-airline'
" eval
Bundle 'nono/vim-handlebars'
Bundle 'kchmck/vim-coffee-script'

  " set path to js-beautify file 
let g:jsbeautify_file = fnameescape(fnamemodify(expand("<sfile>"), ":h")."/bundle/js-beautify/beautify.js") 
"
" Other langs
" 
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
"Bundle 'tpope/vim-markdown'
" OpenScad text editing 
Bundle 'sirtaj/vim-openscad'

Plugin 'fatih/vim-go'

"...All your other bundles...
Bundle 'vim-scripts/SyntaxRange'
Plugin 'NLKNguyen/papercolor-theme'
"
if iCanHazVundle == 0
	echo "Installing Bundles, please ignore key map error messages"
	echo ""
	:BundleInstall
endif
" Setting up Vundle - the vim plugin bundler end

" Bundle specific configuration

" These are the tweaks I apply to YCM's config, you don't need them but they might help.
" YCM gives you popups and splits by default that some people might not like, so these should tidy it up a bit for you.
"let g:ycm_add_preview_to_completeopt=0
"let g:ycm_confirm_extra_conf=0
"set completeopt-=preview