#!/bin/sh

# command-t
#rvm use 1.8.7
cd ~/.vim/bundle/command-t/ruby/command-t
ruby extconf.rb
make
# tern
cd ~/.vim/bundle/tern_for_vim
npm install

# YCM
cd ~/.vim/bundle/YouCompleteMe
./install.sh --clang-completer


