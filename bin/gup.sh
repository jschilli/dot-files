#!/bin/sh
args=("$@")
 
git_repo_folder=${args[0]}/.git/
 
branch_git_cmd="git --git-dir $git_repo_folder symbolic-ref HEAD 2> /dev/null"
branch=$(eval $branch_git_cmd | sed -e 's/refs\/heads\///')
 
git --git-dir $git_repo_folder fetch origin;
git --git-dir $git_repo_folder rebase -p origin/${branch};
git --git-dir $git_repo_folder fetch origin;

